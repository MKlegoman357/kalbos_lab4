% 10. Suskaičiuokite dviejų dimensijų sąrašo bendrą elementų ilgį

length_2_dim([], 0).

length_2_dim([Head|Tail], Length) :-
    length(Head, HeadLength),
    length_2_dim(Tail, TailLength),
    Length is HeadLength + TailLength.

%length_2_dim([], Length). % Length = 0
%length_2_dim([[1], [2]], Length). % Length = 2
%length_2_dim([[1], [2, 3]], Length). % Length = 3
%length_2_dim([[1], [2, 3], [4, 5, 6]], Length). % Length = 6
% 11. Padidinkite sąrašo elementų skaičių pagal pirmą skaičių, pvz.: [[2, "a"], [3, 1]] -> [["aa"][111]]

concat_atom_elements(1, Element, Element).

concat_atom_elements(Amount, Element, Result) :-
    NextAmount is Amount - 1,
    Amount > 0,
    concat_atom_elements(NextAmount, Element, NextResult),
    atom_concat(Element, NextResult, Result).

concat_elements(Amount, Element, Result) :-
    concat_atom_elements(Amount, Element, AtomResult),
    (
    	integer(Element), atom_number(AtomResult, Result);
        string(Element), atom_string(AtomResult, Result);
    	Result = AtomResult
    ).

flatten_list([], []).

flatten_list([[Amount,Element]|Tail], FlattenedList) :-
    concat_elements(Amount, Element, ConcatResult),
    flatten_list(Tail, TailResult),
    append([[ConcatResult]], TailResult, FlattenedList).

flatten_list([[2,"a"],[3,1],[4,1.1]], Result). % Result = [["aa"], [111], ['1.11.11.11.1']]